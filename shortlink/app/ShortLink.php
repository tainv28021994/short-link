<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortLink extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'code',
        'link'
    ];
}
